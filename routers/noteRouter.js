const express = require('express');
const router = express.Router();

const { addNote, getNotes, deleteNote, getNote, updateNote, completeNote } = require('../controllers/noteController');
const authMiddleware = require('../middlewares/authMiddleware');

router.post('/notes', authMiddleware, addNote);
router.get('/notes', authMiddleware, getNotes);
router.delete('/notes/:id', authMiddleware, deleteNote);
router.get('/notes/:id', authMiddleware, getNote);
router.put('/notes/:id', authMiddleware, updateNote);
router.patch('/notes/:id', authMiddleware, completeNote);

module.exports = router;
