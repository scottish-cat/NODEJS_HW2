const mongoose = require('mongoose');

module.exports = mongoose.model('note', {
    userId: {
        required: true,
        type: String,
    },
    completed: {
        required: true,
        type: Boolean,
    },
    text: {
        required: true,
        type: String,
    },
    createdDate: {
        required: true,
        type: String,
    },
});
