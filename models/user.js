const mongoose = require('mongoose');

module.exports = mongoose.model('user', new mongoose.Schema({
    username: {
        required: true,
        type: String,
        unique: true,
    },
    password: {
        required: true,
        type: String,
    },
    createdDate: {
        required: true,
        type: String,
    },
}));
