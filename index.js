const express = require('express');
const mongoose = require('mongoose');
const app = express();

const { port } = require('./config/server');
const { host, databaseName, username, password } = require('./config/database');

const userRouter = require('./routers/userRouter');
const authRouter = require('./routers/authRouter');
const noteRouter = require('./routers/noteRouter');

mongoose.connect(`mongodb+srv://${username}:${password}@${host}/${databaseName}?retryWrites=true&w=majority`, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
  useCreateIndex: true
});

app.use(express.json());
app.use('/api', authRouter);
app.use('/api', userRouter);
app.use('/api', noteRouter);

app.listen(process.env.PORT || port, () => {
    console.log(`Server is listening on port ${port}.`);
});
