# NODEJS_HW2

Use NodeJS to implement CRUD API for notes.

_Requirements:_
- Use express to implement web-server;
- Use express Router for scaling your app and MVC pattern to organise project structure;
- Use jsonwebtoken package for jwt authorization;
- Ability to run server on port which is defined as PORT env variable;
- Mandatory npm start script.


_Acceptance criteria:_
- Ability to register users;
- User is able to login into the system;
- User is able to view only personal notes;
- User is able to add, delete personal notes;
- User can check/uncheck any note;
- User can manage notes and personal profile only with valid JWT token in request.


_Optional criteria:_
1. User is able to view his profile info;
2. User can change his profile password;
3. User can delete personal account;
4. Ability to edit personal notes text;
5. Simple UI for your application(would be a big plus).
