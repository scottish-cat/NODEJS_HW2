const User = require('../models/user');

module.exports.getUser = (request, response) => {
    User.findById(request.user._id).exec()
        .then(user => {
            return response.status(200).json({         
                _id: user._id,
                username: user.username,
                createdDate: new Date(user.createdDate) 
            })
        })
        .catch(err => {
            return response.status(500).json({ message: err.message });
        })
}

module.exports.deleteUser = (request, response) => {
    User.findByIdAndDelete(request.user._id).exec()
        .then(() => {
            return response.status(200).json({ message: "Success" });
        })
        .catch(err => {
            return response.status(500).json({ message: err.message });
        })
}

module.exports.updatePassword = (request, response) => {
    const { oldPassword, newPassword } = request.body;

    if (!oldPassword || !newPassword) {
        return response.status(400).json({ message: "No old password or new password provided." });
    } else if (oldPassword !== request.user.password) {
        return response.status(400).json({ message: "Wrong current password provided." });
    }

    User.findByIdAndUpdate(request.user._id, { password: newPassword }).exec()
        .then(user => {
            return response.status(200).json({ message: "Success" });
        })
        .catch(err => {
            return response.status(500).json({ message: err.message });
        })
}
