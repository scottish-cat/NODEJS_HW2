const Note = require('../models/note');
const user = require('../models/user');

module.exports.addNote = (request, response) => {
    const { text } = request.body;

    const note = new Note({userId: request.user._id, completed: false, text: text || '', createdDate: new Date() });
    note.save()
        .then(() => {
            return response.status(200).json({ message: 'Success' });
        })
        .catch(err => {
            return response.status(500).json({ message: err.message });
        })
}

module.exports.getNotes = (request, response) => {
    Note.find({userId: request.user._id}).exec()
        .then(notes => {
            const result = notes.map(note => {
                return {
                    _id: note._id, 
                    userId: note.userId, 
                    completed: note.completed, 
                    text: note.text, 
                    createdDate: new Date(note.createdDate)
                }
            });

            return response.status(200).json({ notes: result });
        })
        .catch(err => {
            return response.status(500).json({ message: err.message });
        })
}

module.exports.deleteNote = (request, response) => {
    const { id } = request.params;

    Note.findByIdAndDelete(id).exec()
        .then(() => {
            return response.status(200).json({ message: "Success" });
        })
        .catch(err => {
            return response.status(500).json({ message: err.message });
        })
}


module.exports.getNote = (request, response) => {
    const { id } = request.params;

    Note.findById({ _id: id }).exec()
        .then(note => {
            const result = {
                _id: note._id, 
                userId: note.userId, 
                completed: note.completed, 
                text: note.text, 
                createdDate: new Date(note.createdDate)
            }
            
            if (result.userId !== request.user._id) {
                return response.status(400).json({ message: "Not valid note identifier." });
            }

            return response.status(200).json({ note: result });
        })
        .catch(err => {
            return response.status(500).json({ message: err.message });
        })
}

module.exports.updateNote = (request, response) => {
    const { id } = request.params;
    const { text } = request.body;

    if (!text) {
        return response.status(400).json({ message: "No new text provided." });
    }

    Note.findByIdAndUpdate(id, { text }).exec()
        .then(() => {
            return response.status(200).json({ message: "Success" });
        })
        .catch(err => {
            return response.status(500).json({ message: err.message });
        })
}

module.exports.completeNote = (request, response) => {
    const { id } = request.params;

    Note.findById(id).exec()
        .then(note => {
            note.completed = !note.completed;
            note.save()
                .then(() => {
                    return response.status(200).json({ message: "Success" });
                })
                .catch(err => {
                    return response.status(500).json({ message: err.message });
                })
        })
        .catch(err => {
            return response.status(500).json({ message: err.message });
        })
}
