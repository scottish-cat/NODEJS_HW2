const jwt = require('jsonwebtoken');
const User = require('../models/user');
const { secret } = require('../config/auth');

module.exports.register = (request, response) => {
    const { username, password } = request.body;

    if (!username || !password) {
        return response.status(400).json({ message: "No username or password provided." });
    }

    const user = new User({ username, password, createdDate: new Date() });
    user.save()
        .then(() => {
            response.status(200).json({ message: 'Success' });
        })
        .catch(err => {
            response.status(500).json({ message: err.message });
        })
}

module.exports.login = (request, response) => {
    const { username, password } = request.body;

    User.findOne({ username, password }).exec()
        .then(user => {
            if (!user) {
                return response.status(400).json({ message: `User with such credentials wasn't found` });
            } 

            return response.status(200).json({ jwt_token: jwt.sign(JSON.stringify(user), secret) });

        })
        .catch(err => {
            return response.status(500).json({ message: err.message });
        })
}
